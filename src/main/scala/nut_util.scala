package ut_nutshell

import device.AXI4VGA
import difftest.DifftestModule
import nutcore.NutCoreConfig
import sim.SimTop
import system.NutShell
import top._

object comm {
  def init(args: Array[String]): Unit = {
    def parseArgs(info: String, args: Array[String]): String = {
      var target = ""
      for (arg <- args) {
        if (arg.startsWith(info + "=") == true) { target = arg }
      }
      require(target != "")
      target.substring(info.length() + 1)
    }
    val board = parseArgs("BOARD", args)
    val core = parseArgs("CORE", args)

    val s = (board match {
      case "sim"    => Nil
      case "pynq"   => PynqSettings()
      case "axu3cg" => Axu3cgSettings()
      case "PXIe"   => PXIeSettings()
    }) ++ (core match {
      case "inorder"  => InOrderSettings()
      case "ooo"      => OOOSettings()
      case "embedded" => EmbededSettings()
    })
    s.foreach { Settings.settings += _ }
    println(Settings)
    println("====== Settings = (" + board + ", " + core + ") ======")
    Settings.settings.toList.sortBy(_._1)(Ordering.String).foreach {
      case (f, v: Long) =>
        println(f + " = 0x" + v.toHexString)
      case (f, v) =>
        println(f + " = " + v)
    }
    return println("================================================")
  }
}
