# mcv-ut

#### 介绍
NutShell, NanHu-G 等 IP UT分解

#### 生成 DUT
```
git clone https://gitee.com/yaozhicheng/mcv-ut.git
cd mcv-ut
make all
```

在build/NetShell目录下生成：Cache.v, TLB.v 等DUT 待验证模块

#### 测试点分解

Cache 测试点分解文档：[https://sqeeaktlyfy.feishu.cn/docx/ZDmvdldVFo040Wxu9jecP26onXc?from=from_copylink](https://sqeeaktlyfy.feishu.cn/docx/ZDmvdldVFo040Wxu9jecP26onXc?from=from_copylink)
TLB 测试点分解文档：[TBD](TBD)

