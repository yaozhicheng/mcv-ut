
DIR_BUILD=build
DIR_BUILD_NUTSHELL=$(DIR_BUILD)/NutShell
DIR_BUILD_XS_CPL2=$(DIR_BUILD)/CoupledL2

.DEFAULT_GOAL = help

help:
	@echo "example: make init; make all[nt_cache, nt_tlb, nt_tlbemb, ...]"

init:
	git submodule update --init --recursive

nt_sbus:
	mkdir -p $(DIR_BUILD_NUTSHELL)
	mill --no-server -d ut.runMain ut_nutshell.SBusMain  -td $(DIR_BUILD_NUTSHELL) --output-file SimpleBus BOARD=sim CORE=inorder

utest:
	mkdir -p $(DIR_BUILD_NUTSHELL)
	mill --no-server -d tt.test

nt_cache:
	mkdir -p $(DIR_BUILD_NUTSHELL)
	mill --no-server -d ut.runMain ut_nutshell.CacheMain  -td $(DIR_BUILD_NUTSHELL) --output-file Cache BOARD=sim CORE=inorder

nt_xcache:
	mkdir -p $(DIR_BUILD_NUTSHELL)
	mill --no-server -d ut.runMain ut_nutshell.XDumyCacheMain  -td $(DIR_BUILD_NUTSHELL) --output-file XCache BOARD=sim CORE=inorder

nt_soc:
	mkdir -p $(DIR_BUILD_NUTSHELL)
	mill --no-server -d ut.runMain ut_nutshell.XDumySoc  -td $(DIR_BUILD_NUTSHELL) --output-file XDumySoc BOARD=sim CORE=inorder

nt_tlb:
	mkdir -p $(DIR_BUILD_NUTSHELL)
	mill --no-server -d ut.runMain ut_nutshell.TLBMain  -td $(DIR_BUILD_NUTSHELL) --output-file TLB BOARD=sim CORE=inorder

nt_tlbemb:
	mkdir -p $(DIR_BUILD_NUTSHELL)
	mill --no-server -d ut.runMain ut_nutshell.TLBEmbMain  -td $(DIR_BUILD_NUTSHELL) --output-file TLBEmb BOARD=sim CORE=inorder

nt_axi4ram:
	mkdir -p $(DIR_BUILD_NUTSHELL)
	mill --no-server -d ut.runMain ut_nutshell.AXI4RAMMain  -td $(DIR_BUILD_NUTSHELL) --output-file AXI4RAM BOARD=sim CORE=inorder

xs_cpl2:
	mkdir -p $(DIR_BUILD_XS_CPL2)
	cd CoupledL2 && make init -j`nproc`
	cd CoupledL2 && sed -i "/dataBytes/d" src/test/scala/TestTop.scala
	cd CoupledL2 && make test-top-fullsys -j`nproc`
	cp CoupledL2/build/TestTop.v $(DIR_BUILD_XS_CPL2)/CoupledL2FullSys.v
	sed -i "/LogHelper.v/d" $(DIR_BUILD_XS_CPL2)/CoupledL2FullSys.v
	sed -i "/STD_CLKGT_func.v/d" $(DIR_BUILD_XS_CPL2)/CoupledL2FullSys.v
	sed -i "s/\`SIM_TOP_MODULE_NAME.*\;/0\;/g" $(DIR_BUILD_XS_CPL2)/CoupledL2FullSys.v

clean:
	rm -rf $(DIR_BUILD)

all: init nt_cache nt_tlb nt_tlbemb xs_cpl2 nt_axi4ram
